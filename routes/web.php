<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/','InformeController@index');


Route::get('/create', function () {
    return view('index1');
});

Route::get('/informes', 'InformeController@index')-> name('index');

Auth::routes();

Route::get('/', 'HomeController@index2')->name('index2');


Route::get('/home', 'HomeController@index')->name('home');
Route::post('subir_foto', 'HomeController@subir_foto');

Route::post('/informes', 'InformeController@store')->name('store');
// Route::resource('/informes', 'InformeController');

// Route::get('/create', function(){
//     return view('index1');
// })

Route::get('/imageGrayScale', ['as' => 'imageGrayscale', 'uses' => 'ImageGrayController@index']);
Route::post('/imageGrayscale', ['as' => 'imageGrayscale', 'uses' => 'ImageGrayController@imageGrayscale']);

// Route::get('/PdfDemo', 'PdfDemoController@index')->name('PdfDemo');
Route::get('/PdfDemo', ['as' => 'PdfDemo', 'uses' => 'PdfDemoController@index']);

Route::get('/sample-pdf', ['as' => 'SamplePDF', 'uses' => 'PdfDemoController@samplePDF']);

Route::get('/save-pdf', ['as' => 'SavePDF', 'uses' => 'PdfDemoController@savePDF']);

Route::get('/download-pdf', ['as' => 'DownloadPDF', 'uses' => 'PdfDemoController@downloadPDF']);

Route::get('/qr_generate', ['as' => 'qr_generate', 'uses' => 'PdfDemoController@qr_generate']);



Route::get('/imprimir', ['as' => 'imprimir', 'uses' => 'PdfDemoController@imprimir']);
// rutas pdf
Route::get('/077', ['as' => 'imprimir', 'uses' => 'PdfDemoController@PDF077']);
Route::get('/078', ['as' => 'imprimir', 'uses' => 'PdfDemoController@PDF078']);
Route::get('/079', ['as' => 'imprimir', 'uses' => 'PdfDemoController@PDF079']);
Route::get('/080', ['as' => 'imprimir', 'uses' => 'PdfDemoController@PDF080']);
// fin rutas pdf


Route::get('/html-to-pdf', ['as' => 'htmlToPDF', 'uses' => 'PdfDemoController@HtmlToPDF']);