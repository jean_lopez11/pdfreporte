@extends('layouts.app')

@section('content')

<style type="text/css">
	h3.center-text{
		text-align: center;
	}

</style>

<div class="container">
	<h3 class="center-text">GENERADO PDF CON TCPDF</h3>
	<div class="row">
		<div class="col-md-offset-2 col-md-8">
			<br><br>	<br>
			<table cellspacing="3" cellpadding="5" width="100%">
				<tr>
					<td>
						<div class="form-group">
							<a href="077" class="btn btn-primary">
								077 PDF
							</a>
						</div>
					</td>
					<td>
						<div class="form-group">
							<a href="078" class="btn btn-primary">
								078 PDF
							</a>
						</div>
					</td>
										<td>
						<div class="form-group">
							<a href="079" class="btn btn-primary">
								079 PDF
							</a>
						</div>
					</td>
										<td>
						<div class="form-group">
							<a href="080" class="btn btn-primary">
								080 PDF
							</a>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="form-group">
							<a href="sample-pdf" class="btn btn-primary">
								GENERAR PDF
							</a>
						</div>
					</td>

					<td width="25%">	
						<div class="form-group">
							<a href="imprimir" class="btn btn-primary">IMPRMIR PDF </a>
						</div>
					</td>
					<td width="25%">	
						<div class="form-group">
							<a href="save-pdf" class="btn btn-primary">SAVE SAMPLE PDF </a>
						</div>
					</td>
					<td width="25%">	
						<div class="form-group">
							<a href="download-pdf" class="btn btn-primary">DOWNLOAD SAMPLE PDF </a>
						</div>
					</td>
					<td width="25%">	
						<div class="form-group">
							<a href="html-to-pdf" class="btn btn-primary">html to SAMPLE PDF </a>
						</div>
					</td>

				</tr>


			</table>
		</div>
	</div>
</div>
@endsection