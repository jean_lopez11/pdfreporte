
    <!DOCTYPE html>
    <html>
    <head>
    <meta charset="utf-8">
    <link rel="stylesheet"  href="pdf.css">
    <link rel="stylesheet" href="css/pdf.css">
    </head>
    <body>
    
    <div>
        <table width="100%">
                <tr>    
                    <th class="title-style">
                        <strong class="title-pdf">  HISTORIA CLINICA PRE - OCUPACIONAL O DE INICIO </strong>
                    </th>
                </tr>
        </table>
    </div>

 
    <table width="100%" class="table-mtv" border="1">
        <tr >
            <th class="des-style" colspan="6"  >
                <strong class="des-detail"> A. DATOS DEL ESTABLECIMIENTO - EMPRESA Y USUARIO</strong>
            </th>
        </tr>
        <tr>
            <td class="camp-empres">
                <b class="camp-cbz">INSTITUCIÓN DEL SISTEMA O NOMBRE DE LA EMPRESA</b>
            </td> 
            <td class="camp-ruc"> 
                <b>RUC</b>
            </td>
            <td  class="camp-ciu">
                <b class="camp-cbz">CIIU</b>
            </td>
            <td class="camp-es">
                <b>ESTABLECIMIENTO DE SALUD</b>
            </td>
            <td class="camp-hc">
                <b class="camp-cbz">NÚMERO DE HISTORIA CLÍNICA </b>
            </td>
            <td  class="camp-na" width="10%" >
                <b class="camp-cbz">NÚMERO DE ARCHIVO</b>
            </td>
        </tr>
        <tbody width="100%">
            <tr>
                <td class="result-camp-1">
                    empresa_nombrefffffffffffffffffffffffffff fffffffffffffffddsssssasasasassss
                </td>
                <td class="result-camp-2" > empresa_ruc 
                </td>
                <td class="result-camp-3"  >
                 empresa_ciu
                </td>
                <td class="result-camp-4"> consultorio->nombre </td>
                <td class="result-camp-5" >
                    num_historia_clinica 
                </td>

                <td class="result-camp-6">
                    num_archivo 
                </td>        
            </tr>  

        </tbody>  
    </table>
    <table class="table-mtv" border="1"  >
        <tr>
            <th class="camp-1"  rowspan="3"><br> <br><br>PRIMER APELLIDO </th>
            <th class="camp-1"  rowspan="3"><br> <br><br>SEGUNDO APELLIDO</th>
            <th class="camp-1"  rowspan="3"><br> <br><br>PRIMER NOMBRE </th>
            <th class="camp-1"  rowspan="3"><br> <br><br>SEGUNDO NOMBRE</th>
            <th class="camp-1"  rowspan="3"><br> <br><br>SEXO</th>
            <th class="camp-2"  rowspan="3"><br> <br><br>EDAD (AÑOS)</th>
            <th class="camp-3"  colspan="5" rowspan="1">RELIGIÓN</th>
            <th class="camp-4"  rowspan="3"><br> <br><br>GRUPO SANGUÍNEO</th>
            <th class="camp-4"  rowspan="3"><br> <br><br>LATERALIDAD</th>
        </tr>
      
        <tr > 
            <th  class="camp-7"  colspan="1" rowspan="2" ><p class="verticalText">Católica</p>
            </th>
            <th class="camp-7"  colspan="1" rowspan="2"> 
                <p class="verticalText"> Evangélica </p>
            </th>
            <th class="camp-7" colspan="1" rowspan="2" ><p class="verticalText"> Testigos de Jehová</p> 
            </th>
            <th class="camp-7" colspan="1" rowspan="2" ><p class="verticalText">Mormona</p>
            </th>
            <th class="camp-7" colspan="1" rowspan="2" ><p class="verticalText"> Otras</p>
            </th>
       
        </tr>
        <tr>
            <tbody>
                <tr>
                    <td class="result-camp-6">paterno</td>
                    <td class="result-camp-6">materssssssssssss</td>
                    <td class="result-camp-6">nombres_1</td>
                    <td class="result-camp-6">nombres_2</td>
                    <td class="result-camp-6">persona->sexo </td>
                    <td class="result-camp-6">persona->edad </td>
                    <td class="result-camp-7">CATOLICA </td>
                    <td  class="result-camp-7">EVANGÉLICA</td>
                    <td  class="result-camp-7">TESTIGO DE JEOVA</td>
                    <td  class="result-camp-7">MORMONA</td>
                    <td  class="result-camp-7">OTRO</td>
                    <td class="result-camp-6">grupo_sanguineo </td>
                    <td class="result-camp-6">lateralidad </td>
                </tr>
            </tbody>
        </tr>
    </table>


    <table class="table-dtp" border="1" width="100%">
        <tr>
            <th  class="camp-3"colspan="5" rowspan="1">ORIENTACIÓN SEXUAL </th>
            <th class="camp-3" colspan="5" rowspan="1">IDENTIDAD DE GÉNERO</th>
            <th class="camp-4" colspan="4" rowspan="1">DISCAPACIDAD</th>
            <th class="camp-9" colspan="1" rowspan="3">FECHA INGRESO AL TRABAJO (aaaa/mm/dd)</th>
            <th class="camp-9" colspan="1" rowspan="3">PUESTO DE
                TRABAJO
            (CIUO)</th>
            <th class="camp-9" colspan="1" rowspan="3">ÁREA DE TRABAJO</th>
            <th  class="camp-5" colspan="1" rowspan="3">ACTIVIDADES RELEVANTES AL PUESTO DE TRABAJO A OCUPAR</th>
        </tr>
        <tr> 

            <th rowspan="2" colspan="1" class="camp-14"> <p class="verticalText">Lesbiana</p> </th>
            <th rowspan="2" colspan="1" class="camp-14"> <p class="verticalText">Gay</p></th>
            <th rowspan="2" colspan="1" class="camp-14"> <p class="verticalText">Bisexual</p></th>
            <th rowspan="2" colspan="1" class="camp-14"><p class="verticalText">Heterosexual</p></th>
            <th rowspan="2" colspan="1" class="camp-14"><p class="verticalText">No sabe/ no responde</p></th>
            
            <th rowspan="2" class="camp-9"><p class="verticalText">Femenino</p></th>
            <th rowspan="2" class="camp-9"><p class="verticalText">Masculino</p></th>
            <th rowspan="2" class="camp-9"><p class="verticalText">Trans-femenino</p></th>
            <th rowspan="2" class="camp-9"> <p class="verticalText">Trans-masculino</p></th>
            <th rowspan="2" class="camp-9"><p class="verticalText">No sabe/no responde</p></th>

            <th rowspan="2" class="camp-11"> SI </th>
            <th rowspan="2" class="camp-11">NO</th>
            <th rowspan="2" class="camp-11">TIPO</th>
            <th rowspan="2" class="camp-11">%</th>
        </tr>
        <tr>
            <tbody>
                <tr>
                    <td class="result-camp-7">p</td>
                    <td class="result-camp-7">m</td>
                    <td class="result-camp-7">n</td>
                    <td class="result-camp-7">n</td>
                    <td class="result-camp-7">p </td>
                    <td class="result-camp-7">p</td>
                    <td class="result-camp-7">C </td>
                    <td class="result-camp-7">E</td>
                    <td class="result-camp-7">D</td>
                    <td class="result-camp-7">M</td>
                    <td class="result-camp-7">O</td>
                    <td class="result-camp-7">g </td>
                    <td class="result-camp-7">l </td>
                    <td class="result-camp-7">c </td>
                    <td class="result-camp-9">p </td>
                    <td class="result-camp-9">DEPARTAMENTO DE ELECTRICIDADssssssssssssssssss </td>
                    <td class="result-camp-9">DEPARTAMENTO DE ELECTRICIDA </td>
                    <td class="result-camp-9">DEPARTAMENTO DE ELECTRICIDA DEPARTAMENTO DE ELECTRICIDA DEPARTAMENTO DE ELECTRICIDA </td>
                </tr>
            </tbody>
        </tr>
    </table>

    <br>  <br> 


    <table class="table-mtv" width="100%">  
        <tr>
            <th  width="100%"  class="des-style th-sbdr" >
                <strong class="des-detail">
                    B. MOTIVO DE CONSULTA 
                </strong>
            </th>
            <th  width="100%"  class="des-style th-sbdr">
                <strong class="des-detail">ANOTAR LA CAUSA DEL PROBLEMA EN LA VERSIÓN DEL INFORMANTE 
                </strong>
            </th>
        </tr>
        <tbody>
            <tr>
                <td  colspan="2" class="result-camp-8">
                  PACIENTE CRONICO
              </td>               
          </tr>  
        </tbody>  
    </table>

    <br>
    <table class="table-mtv" border="1" width="100%">  
        <tr>
            <th  width="100%"  class="des-style th-sbdr" >
                <strong class="des-detail">
                    C. ANTECEDENTES PERSONALES
                </strong>
            </th>
        </tr>
        <tr>
            <td width="100%"  class="camp-3" >
                ANTECEDENTES CLÍNICOS Y QUIRÚRGICOS
            </td>               
        </tr>  
        <tbody>
            <tr>
                <td  class="result-camp-8">
                    PADRES DIABETICOS
                </td>
            </tr>  
        </tbody>  
    </table>

    <table class="table-dtp" border="1">
        <tr>
            <th colspan="14" class="camp-12"> ANTECEDENTES GINECO OBSTÉTRICOS
            </th>
        </tr>
        <tr>
            <th colspan="1" rowspan="2" class="camp-2" >MENARQUÍA </th>
            <th colspan="1" rowspan="2" class="camp-2">CICLOS</th>
            <th width="10%" colspan="1" rowspan="2" class="camp-2">FECHA DE ULTIMA MENSTRUACIÓN (aaaa/mm/dd)
            </th>
            <th width="6%" colspan="1" rowspan="2" class="camp-2">GESTAS</th>
            <th width="6%" colspan="1" rowspan="2" class="camp-2">PARTOS</th>
            <th width="8%" colspan="1" rowspan="2" class="camp-2">CESÁREAS</th>
            <th  width="7%" colspan="1" rowspan="2" class="camp-2">ABORTOS</th>

            <th width="13%" colspan="2" rowspan="1" class="camp-2">HIJOS</th>
            <th width="8%" colspan="2" rowspan="1" class="camp-2">VIDA SEXUAL
            ACTIVA</th>
            <th  width="30%" colspan="3" rowspan="1" class="camp-2">MÉTODO DE PLANIFICACIÓN FAMILIAR</th>
        </tr>

        <tr> 
            <td class="font-size6pt">VIVOS</td>
            <td class="font-size6pt">MUERTOS</td>
            <td class="font-size6pt">SI</td>
            <td class="font-size6pt">NO</td>
            <td class="font-size6pt">SI</td>
            <td class="font-size6pt">NO</td>
            <td class="font-size6pt">TIPO</td>
        </tr>
        <tr> 
            <td class="font-size6pt">abcsdfg</td>
            <td class="font-size6pt">abcsdfg</td>
            <td class="font-size6pt">abcsdfg</td>
            <td class="font-size6pt">abcsdfg</td>
            <td class="font-size6pt">abcsdfg</td>
            <td class="font-size6pt">abcsdfg</td>
            <td class="font-size6pt">abcsdfg</td>
            <td class="font-size6pt">abcsdfg</td>
            <td class="font-size6pt">abcsdfg</td>
            <td class="font-size6pt">abcsdfg</td>
            <td class="font-size6pt">abcsdfg</td>
            <td class="font-size6pt">abcsdfg</td>
            <td class="font-size6pt">abcsdfg</td>
            <td class="font-size6pt">abcsdfg</td>
        </tr>
    </table>

    <table width="100%" class="table-mtv center-txt" border="1" >  
        <tr>
            <td  class="camp-3">
              EXÁMENES REALIZADOS</b>
            </td> 
            <td class="camp-7"> SI </td>
            <td class="camp-7"> NO </b>
            </td>
            <td class="camp-2"> TIEMPO (años)</td>
            <td class="camp-13"> RESULTADO </td>

            <td  class="camp-10"> EXÁMENES REALIZADOS </td>
            <td class="camp-7"> SI </td>
            <td  class="camp-7">
              NO
            </td>
            <td class="camp-2"> TIEMPO (años)</td>
            <td  class="camp-13">RESULTADO </td>
        </tr>

        <tbody>
            <tr>
                <td class="break-txt font-size7pt">PAPANICOLAOU</td>
                <td class="break-txt font-size7pt">pa</td>
                <td class="break-txt font-size7pt" class="break-txt">fff</td>
                <td class="break-txt font-size7pt"> dd</td>
                <td class="break-txt font-size7pt"> ddd</td>
                <td class="break-txt font-size7pt"> MAM</td>
                <td class="break-txt font-size7pt"> ddd</td>
                <td class="break-txt font-size7pt"> ddd</td>
                <td class="break-txt font-size7pt">ddd</td>
                <td class="break-txt font-size7pt">ddd</td>        
            </tr>  
            <tr>
                <td class="break-txt font-size7pt">COLPOSCOPIA</td>
                <td class="break-txt font-size7pt"  >CO</td>
                <td class="break-txt font-size7pt" >COL</td>
                <td class="break-txt font-size7pt" >COLPOSCOPIA</td>
                <td class="break-txt font-size7pt">COLPOSCOPIA</td>        
                <td class="break-txt font-size7pt">  MAMOGRAFÍA</td>
                <td class="break-txt font-size7pt" >MA</td>
                <td class="break-txt font-size7pt" > MA</td>
                <td class="break-txt font-size7pt" > fff </td>
                <td class="break-txt font-size7pt">dd</td>        
            </tr>  
        </tbody>  
    </table>
    
    <table width="100%" class="table-dtp" border="1">
        <tr>
            <th colspan="10" rowspan="1" class="camp-12"> ANTECEDENTES REPRODUCTIVOS MASCULINOS
            </th>

        </tr>
        
        <tr>
            <td rowspan="2" class="camp-3">
              EXAMENES REALIZADOS</b>
            </td> 
            <td rowspan="2" class="camp-7"> SI </td>
            <td rowspan="2" class="camp-7"> NO </b>
            </td>
            <td rowspan="2" class="camp-2"> TIEMPO (años)</td>
            <td rowspan="2" class="camp-13"> RESULTADO </td>

            <td rowspan="1" colspan="5" class="camp-10"> MÉTODO DE PLANIFICACIÓN FAMILIAR </td>
        </tr>
        <tr>
            <td rowspan="1" class="camp-7"> SI </td>
            <td rowspan="1" class="camp-7">
              NO
            </td>
            <td rowspan="1" class="camp-2"> TIPO </td>
            <td rowspan="1" class="camp-13">   VIVOS</td>
            <td rowspan="1" class="camp-13">   MUERTOS</td>
        </tr>

        <tbody>
            <tr>
                <td class="break-txt font-size7pt">ECOPROSTATICO</td>
                <td class="break-txt font-size7pt">pa</td>
                <td class="break-txt font-size7pt" class="break-txt">fff</td>
                <td class="break-txt font-size7pt"> dd</td>
                <td class="break-txt font-size7pt"> ddd</td>
                <td class="break-txt font-size7pt"> MAM</td>
                <td class="break-txt font-size7pt"> ddd</td>
                <td class="break-txt font-size7pt"> ddd</td>
                <td class="break-txt font-size7pt">ddd</td>
                <td class="break-txt font-size7pt">ddd</td>        
            </tr>  
            <tr>
                <td class="break-txt font-size7pt">PROTA</td>
                <td class="break-txt font-size7pt"  >CO</td>
                <td class="break-txt font-size7pt" >COL</td>
                <td class="break-txt font-size7pt" >COLPOSCOPIA</td>
                <td class="break-txt font-size7pt">COLPOSCOPIA</td>        
                <td class="break-txt font-size7pt">  MAM</td>
                <td class="break-txt font-size7pt" >MA</td>
                <td class="break-txt font-size7pt" > MA</td>
                <td class="break-txt font-size7pt" > fff </td>
                <td class="break-txt font-size7pt">dd</td>        
            </tr>  
        </tbody>  
    </table>


    <table class="table-mtv" border="1" width="100%">
        <tr>
            <th colspan="7" border="1" width="58.5%" class="camp-12-azul"  >
              HÁBITOS TÓXICOS
            </th>
            <th colspan="5" border="1"  width="41.5%" class="camp-12-azul" >
              ESTILO DE VIDA
            </th>
        </tr>

        <tr>
            <th border="1" class="camp-3 break-txt">
                CONSUMOS NOCIVOS
            </th> 
            <th class="camp-7"> 
                SI
            </th>
            <th  class="camp-7">NO</th>
            <th class="camp-2">TIEMPO DE CONSUMO (meses)</th>
            <th class="camp-2">CANTIDAD </th>
            <th class="camp-10">EX CONSUMIDOR
            </th>
            <th class="camp-4">TIEMPO DE ABSTINENCIA(meses)
            </th>
            <th class="camp-4">ESTILO
            </th>
            <th class="camp-7"> SI </th>
            <th class="camp-7">NO</th>
            <th class="camp-3">¿CUÁL?
            </th>
            <th class="camp-3">TIEMPO/CANTIDAD</b>
            </th>
    </tr>

        <tbody>
            <tr>
                <td class="result-camp-3 break-txt">habi</td>
                <td class="result-camp-7 break-txt">habi</td>
                <td class="result-camp-7 break-txt">habit</td>
                <td class="result-camp-2 break-txt">habit]</td>
                <td class="result-camp-2 break-txt">habit}</td>     
                <td class="result-camp-4 break-txt">habit</td>
                <td class="result-camp-4 break-txt">habiA']</td>
                <td class="result-camp-4 break-txt">ESTILO</td>
                <td class="result-camp-7 break-txt">habi</td>
                <td class="result-camp-7 break-txt">habi</td>
                <td class="result-camp-3 break-txt"> cual </td>
                <td class="result-camp-3 break-txt">habito</td>      
            </tr> 

        </tbody>  
    </table>
    <br>
    <table class="table-mtv" border="1" width="100%">
        <tr>
            <th width="100%" class="camp-12-azul" colspan="11" >D. ANTECEDENTES DE TRABAJO</th>
          </tr>
        <tr>
            <th width="100%" class="camp-12" colspan="11">
              ANTECEDENTES DE EMPLEOS ANTERIORES
          </th>
        </tr>
        <tr>
            <th class="camp-12" colspan="1" rowspan="2">EMPRESA</th>
            <th class="camp-15" colspan="1" rowspan="2">
              PUESTO DE TRABAJO
          </th>
          <th class="camp-16" colspan="1" rowspan="2">
              ACTIVIDADES QUE DESEMPEÑABA 
          </th>
          <th class="camp-10" colspan="1" rowspan="2">
              TIEMPO DE TRABAJO (meses)
          </th>
          <th class="camp-15" colspan="6" rowspan="1">RIESGO</th>
          <th class="camp-16" colspan="1" rowspan="2">OBSERVACIONES</th>
        </tr>
        <tr > 
            <th class="camp-9 "><p class="verticalText margin-30px">FÍSICO</p></th>
            <th class="camp-9"><p class="verticalText margin-30px">MECÁNICO</p></th>
            <th class="camp-9"><p class="verticalText margin-30px">QUÍMICO</p></th>
            <th  class="camp-9"><p class="verticalText margin-30px">BIOLÓGICO</p></th>
            <th class="camp-9"><p class="verticalText margin-30px">ERGONÓMICO</p></th>
            <th class="camp-9"><p class="verticalText margin-30px" >PSICOSOCIAL</p></th>
        </tr>
        <tbody>
            <tr> 
                <td class="font-size6pt">emple</td>
                <td class="font-size6pt">emple</td>
                <td class="font-size6pt">empl}</td>
                <td class="font-size6pt">empl</td>
                <td class="font-size6pt">issriesg</td>
                <td class="font-size6pt"> (issriesg</td>
                <td class="font-size6pt"> (is</td>
                <td class="font-size6pt"> (isri</td>
                <td class="font-size6pt">(isies</td>
                <td class="font-size6pt"> (issig</td>
                <td class="font-size6pt">empleo</td> 
            </tr>
        </tbody>
    </table> 


 
    <table class="tb-border" width="100%">
        <tr>
            <th width="100%" class="camp-12-azul border-cd" colspan="11" rowspan="1">D. ANTECEDENTES DE TRABAJO</th>
        </tr>
        <tr>
            <th width="100%" class="camp-12 border-cd" colspan="11" rowspan="1">ANTEDENTES DE EMPLEOS ANTERIORES</th>
        </tr>
        <tr>
            <th width="100%" colspan="11" rowspan="1">
                <br>
            </th>
        </tr>
        <tr>
            <td width="50%" class="result-camp-10">FUE CALIFICADO POR EL INSTITUTO DE SEGURIDAD SOCIAL CORRESPONDIENTE:</td>
              
            <th class="font-size6pt" width="3%">SI</th>
            <td class="border-cd font-size6pt" width="3%">X</td>
            <th class="font-size6pts" width="20%">ESPECIFICAR:</th>
            <th class="font-size6pt" width="5%">NO</th>
            <td class="border-cd font-size6pt" width="2%">X </td>
            <th class="font-size6pt" width="6%"> FECHA</th>              
            <td class="border-cd font-size6pt" width="5%" >2020</td>
            <td class="border-cd font-size6pt" width="5%" >06 </td>
            <td class="border-cd font-size6pt" width="5%" >1 </td>
            <td class="" width="5%"> </td>
        </tr>
        <tr>
          <th width="50%"><br></th>
          
          <th  width="3%"></th>
          <th width="3%" border="0"></th>
          
          <th  width="20%"></th>
          <th width="5%"></th>
          <th width="2%" border="0"></th>
          <th width="6%"> </th>
          
          <th width="5%" class="font-size6pt"> aaaa</th>
          <th width="5%" class="font-size6pt"> mm</th>
          <th width="5%" class="font-size6pt"> dd</th>
          <th width="5%" class="font-size6pt"> </th>
      </tr>
      <tr>
          <td colspan="11" class="font-size6pt left-txt border-cd">  <strong>Observaciones: </strong> fsdfdsf</td>
      </tr>
   
    </table>
    <table class="tb-border" width="100%">
        <tr>
            <th width="100%" class="camp-12 border-cd" colspan="11" rowspan="1">ENFERMEDADES PROFESIONALES</th>
        </tr>
        <tr>
            <th width="100%" colspan="11" rowspan="1">
                <br>
            </th>
        </tr>
        <tr>
            <td width="50%" class="result-camp-10">FUE CALIFICADO POR EL INSTITUTO DE SEGURIDAD SOCIAL CORRESPONDIENTE:</td>
              
            <th class="font-size6pt" width="3%">SI</th>
            <td class="border-cd font-size6pt" width="3%">X</td>
            <th class="font-size6pts" width="20%">ESPECIFICAR:</th>
            <th class="font-size6pt" width="5%">NO</th>
            <td class="border-cd font-size6pt" width="2%">X </td>
            <th class="font-size6pt" width="6%"> FECHA</th>              
            <td class="border-cd font-size6pt" width="5%" >2020</td>
            <td class="border-cd font-size6pt" width="5%" >06 </td>
            <td class="border-cd font-size6pt" width="5%" >1 </td>
            <td class="" width="5%"> </td>
        </tr>
        <tr>
          <th width="50%"><br></th>
          
          <th  width="3%"></th>
          <th width="3%" border="0"></th>
          
          <th  width="20%"></th>
          <th width="5%"></th>
          <th width="2%" border="0"></th>
          <th width="6%"> </th>
          
          <th width="5%" class="font-size6pt"> aaaa</th>
          <th width="5%" class="font-size6pt"> mm</th>
          <th width="5%" class="font-size6pt"> dd</th>
          <th width="5%" class="font-size6pt"> </th>
      </tr>
      <tr>
          <td colspan="11" class="font-size6pt left-txt border-cd">  <strong>Observaciones: </strong> fsdfdsf</td>
      </tr>
   
    </table>

    <table class="table-mtv" width="100%">  
        <tr>
            <th colspan="10" class="camp-12-azul th-sbdr" >
            E. ANTECEDENTES FAMILIARES (DETALLAR EL PARENTESCO)
            <th colspan="6" class="camp-12-azul th-sbdr">
                <strong class="stl right-txt">MARCAR Y DESCRIBIR ABAJO ANOTANDO EL NÚMERO </strong></th>
            </th>
            
        </tr>
        <tr>     
            <th class="camp-4 break-txt">1. ENFERMEDAD CARDIO-VASCULAR </th>
            <th  class="result-camp-7" width="2%">X </th>
            <th class="camp-4 break-txt">
              2. ENFERMEDAD METABÓLICA
            </th>
            <th  class="result-camp-7" width="2%">X</th>
            <th class="camp-4 break-txt"> 3 ENFERMEDAD NEUROLÓGICA </th>
            <th  class="result-camp-7" width="2%">X</th>
            <th class="camp-4 break-txt">4. ENFERMEDAD ONCOLÓGICA
            </th>
            <th  class="result-camp-7" width="2%">X</th>
            <th class="camp-4 break-txt">5. ENFERMEDAD INFECCIOSA</th>
            <th  class="result-camp-7" width="2%">X</th>
            <th class="camp-4 break-txt">6. ENFERMEDAD HEREDITARIA / CONGÉNITA</th>
            <th  class="result-camp-7" width="2%">X</th>
            <th class="camp-4 break-txt">7. DISCAPACIDADES</th>
            <th  class="result-camp-7" width="2%">X</th>
            <th class="camp-4 break-txt">8. OTROS</th>
            <th  class="result-camp-7" width="2%"> X</th>
        </tr>
        <tbody>  
            <tr>
                <td class="result-camp-4 break-txt left-txt border-cd" colspan="16" width="100%">
                hola mundo
                </td>               
            </tr>      
        </tbody>  
    </table>  






    <h1> table puesto de trabajo</h1>

    <table class="table-mtv" width="100%">  
        <tr>
            <th  width="100%"  class="des-style border-cd" >
                <strong class="des-detail">
                    B. ACTIVIDADES EXTRA LABORALES 
                </strong>
            </th>
        </tr>
        <tbody>
            <tr>
                <td class="result-camp-8">
                  PACIENTE CRONICO
              </td>               
          </tr>  
        </tbody>  
    </table>
    <br>
    <table class="table-mtv" width="100%">  
        <tr>
            <th  width="100%"  class="des-style border-cd" >
                <strong class="des-detail">
                    B. ENFERMEDAD ACTUAL
                </strong>
            </th>
        </tr>
        <tbody>
            <tr>
                <td  class="result-camp-8 border-cd">
                  PACIENTE CRONICO
              </td>               
          </tr>  
        </tbody>  
    </table>
    <br>
    <table class="table-mtv" border="1" width="100%">  
        <tr>
            <th colspan="10" class="camp-12-azul th-sbdr" >
            I. REVISION ACTUAL DE ORGANOS Y SISTEMAS     
        </tr>
        
        <tr>     
            <th class="camp-4 break-txt">1. PIEL - ANEXOS </th>
            <th  class="result-camp-7" width="2%">X </th>
            <th class="camp-4 break-txt">
              3. RESPIRATORIO
            </th>
            <th  class="result-camp-7" width="2%">X</th>
            <th class="camp-4 break-txt"> 5. DIGESTIVO </th>
            <th  class="result-camp-7" width="2%">X</th>
            <th class="camp-4 break-txt">7. MÚSCULO ESQUELÉTICO
            </th>
            <th  class="result-camp-7" width="2%">X</th>
            <th class="camp-4 break-txt">9. HEMO LINFÁTICO</th>
            <th  class="result-camp-7" width="2%">X</th>
        </tr>
        <tr>     
            <th class="camp-4 break-txt"> 2. ÓRGANOS DE LOS SENTIDOS </th>
            <th  class="result-camp-7" width="2%">X </th>
            <th class="camp-4 break-txt">
               4. CARDIO-VASCULAR
            </th>
            <th  class="result-camp-7" width="2%">X</th>
            <th class="camp-4 break-txt"> 6. GENITO - URINARIO </th>
            <th  class="result-camp-7" width="2%">X</th>
            <th class="camp-4 break-txt"> 8. ENDOCRINO
            </th>
            <th  class="result-camp-7" width="2%">X</th>
            <th class="camp-4 break-txt"> 10. NERVIOSO</th>
            <th  class="result-camp-7" width="2%">X</th>
        </tr>
        <tbody>  
            <tr>
                <td class="result-camp-4 break-txt left-txt border-cd" colspan="10" width="100%">
                hola mundo
                </td>               
            </tr>      
        </tbody>  
    
    </table>  
    <br>
    <table class="table-mtv" border="1" width="100%">  
        <tr>
            <th colspan="9" class="camp-12-azul th-sbdr" >
             J. CONSTANTES VITALES Y ANTROPOMETRÍA    
        </tr>
        <tr>     
            <th class="camp-4">  PRESIÓN ARTERIAL (mmHg) </th>
            <th class="camp-4 "> TEMPERATURA (°C) </th>
            <th class="camp-4 ">
               FRECUENCIA CARDIACA (Lat/min)
            </th>
            <th class="camp-4 "> SATURACIÓN DE OXÍGENO (O2%) </th>
            <th class="camp-4 "> FRECUENCIA RESPIRATORIA (fr/min) </th>
            <th class="camp-4 "> PESO (Kg) </th>
            <th class="camp-4 "> TALLA (cm)
            </th>
            <th class="camp-4 "> ÍNDICE DE MASA CORPORAL (kg/m2) </th>
            <th class="camp-4 "> PERÍMETRO ABDOMINAL (cm)</th>
            
        </tr>
        <tbody>  
            <tr>
                <td class="result-camp-4 break-txt border-cd" colspan="" width="100%">
                hola mundo
                </td>        
                <td class="result-camp-4 break-txt border-cd" colspan="" width="100%">
                hola mundo
                </td>
                <td class="result-camp-4 break-txtt border-cd" colspan="" width="100%">
                hola mundo
                </td>
                <td class="result-camp-4 break-txt border-cd" colspan="" width="100%">
                hola mundofffffffffffffffffffff
                </td>
                <td class="result-camp-4 break-txt border-cd" colspan="" width="100%">
                hola mundo
                </td>
                <td class="result-camp-4 break-txtborder-cd" colspan="" width="100%">
                hola mundo
                </td>
                <td class="result-camp-4 break-txt border-cd" colspan="" width="100%">
                hola mundo
                </td>
                <td class="result-camp-4 break-txtt border-cd" colspan="" width="100%">
                hola mundo
                </td>
                <td class="result-camp-4 break-txt border-cd" colspan="" width="100%">
                hola mundo
                </td>

            </tr>      
        </tbody>  
    </table>  
    <br>
    <table class="table-mtv" border="1" width="100%">  
        <tr>
            <th colspan="11" class="camp-12-azul th-sbdr" >
            K. EXAMEN FISICO REGIONAL
        </tr>

        <tr>   
            <th rowspan="2"> <p>PIEL</p> </th>  
            <th class="camp-4 break-txt">1. PIEL - ANEXOS </th>
            <th  class="result-camp-7" width="2%">X </th>
            <th class="camp-4 break-txt">
              3. RESPIRATORIO
            </th>
            <th  class="result-camp-7" width="2%">X</th>
            <th class="camp-4 break-txt"> 5. DIGESTIVO </th>
            <th  class="result-camp-7" width="2%">X</th>
            <th class="camp-4 break-txt">7. MÚSCULO ESQUELÉTICO
            </th>
            <th  class="result-camp-7" width="2%">X</th>
            <th class="camp-4 break-txt">9. HEMO LINFÁTICO</th>
            <th  class="result-camp-7" width="2%">X</th>
        </tr>
        <tr>     
            <th class="camp-4 break-txt"> 2. ÓRGANOS DE LOS SENTIDOS </th>
            <th  class="result-camp-7" width="2%">X </th>
            <th class="camp-4 break-txt">
               4. CARDIO-VASCULAR
            </th>
            <th  class="result-camp-7" width="2%">X</th>
            <th class="camp-4 break-txt"> 6. GENITO - URINARIO </th>
            <th  class="result-camp-7" width="2%">X</th>
            <th class="camp-4 break-txt"> 8. ENDOCRINO
            </th>
            <th  class="result-camp-7" width="2%">X</th>
            <th class="camp-4 break-txt"> 10. NERVIOSO</th>
            <th  class="result-camp-7" width="2%">X</th>
        </tr>
        <tbody>  
            <tr>
                <td class="result-camp-4 break-txt left-txt border-cd" colspan="11" width="100%">
                hola mundo
                </td>               
            </tr>      
        </tbody>  
    
    </table> 



    <table class="table-mtv" border="1">
        <tr>
            <th  colspan="15" class="camp-12-azul" >
              <strong style="color: #000000"> K. EXAMEN FÍSICO REGIONAL</strong>
          </th>
        </tr>
        <tbody>
            <tr>
                <td colspan="1" rowspan="3" class="campa-1t"> 
                  <p>PIEL</p> 
                </td>
                <td class="campa-2" > a. Cicatrices</td>
                <td width="2%" border="1"align="center"> X </td>
                <td width="3%" border="1" align="center" colspan="1" rowspan="3" style="font-size: 6pt;background-color: #d2edf4;"> 3. OIDO </td>
                <td width="18%" border="1" style="font-size:6pt;background-color:#d2edf4;">C. auditivo externo</td>
                <td width="2%" border="1" align="center"> X </td>        
                <td width="3%"  border="1" align="center" colspan="1" rowspan="4" style="font-size: 6pt; background-color: #d2edf4;"> 5. NARIZ </td>
                <td width="16%" border="1" style="font-size:6pt;background-color:#d2edf4;" >a.Tabique</td>
                <td width="2%"  border="1"align="center"> X </td>
                <td width="3%"  border="1" align="center" colspan="1" rowspan="2" style="font-size: 6pt;  background-color: #d2edf4;"  >  8. TORAX </td>
                <td width="16%" border="1" style="font-size:6pt;background-color:#d2edf4;">a. Pulmones
                </td>
                <td width="2%"  border="1"align="center"> X </td>
                <td width="3%"  border="1" align="center" colspan="1" rowspan="2" style="font-size: 6pt;  background-color: #d2edf4;"> 11. PELVIS </td>
                <td width="15%" border="1" style="font-size:6pt;background-color: #d2edf4;">a. Pelvis</td> 
                <td width="2%"  border="1"align="center">{{(isset($ktb_pelvis["a. Pelvis"]))?'X':null}}</td>
            </tr> 
            
            <tr>
                <td class="campa-2">b. Tatuajes</td>
                <td width="2%"  border="1"> X </td>
                <td width="18%" border="1" style="font-size:6pt;background-color:#d2edf4;">b. Pabellón</td>
                <td width="2%"  border="1"> X </td>
                <td width="16%" border="1" style="font-size:6pt;background-color:#d2edf4;">b. Cornetes</td>
                <td width="2%"  border="1" > X </td>
                <td width="16%" border="1" style="font-size:6pt;background-color:#d2edf4;">b. Parrilla Costal</td>
                <td width="2%"  border="1" > X </td>
                <td width="15%" border="1" style="font-size:6pt;background-color:#d2edf4;">b. Genitales</td>
                <td width="2%"  border="1"> X </td>
            </tr>  
            <tr>
                <td class="campa-2"> c. Piel y Faneras</td>
                <td width="2%"  border="1"> X </td>
                <td width="18%"  border="1" style="font-size: 6pt;background-color: #d2edf4;" >c. Tímpanos</td>
                <td width="2%"  border="1"> X </td>
                <td width="16%" border="1" style="font-size:6pt;background-color: #d2edf4;">c. Mucosas</td>
                <td width="2%" border="1"> X </td>
                <td width="3%" border="1" align="center" colspan="1" rowspan="2" style="font-size: 6pt;  background-color: #d2edf4;"  > 9. ABDOMEN </td>
                <td width="16%"  border="1" style="font-size: 6pt;background-color: #d2edf4;">a. Vísceras</td>
                <td width="2%"  border="1"> X</td>
                <td width="3%"  border="1" align="center" colspan="1" rowspan="3" style="font-size: 6pt;  background-color: #d2edf4;"  > 12. EXTREMIDADES    </td>
                <td width="15%" border="1" style="font-size: 6pt;background-color: #d2edf4;">a. Vascular</td>
                <td width="2%" border="1" > X</td>
            </tr>  
            <tr>
                <td width="3%"  border="1" align="center" colspan="1" rowspan="5" style="font-size: 6pt;  background-color: #d2edf4;"  > 2. OJOS</td>
                <td class="campa-2">a. Párpados</td>
                <td width="2%"  border="1"align="center">X</td>
                <td width="3%"  border="1" align="center" colspan="1" rowspan="5" style="font-size: 6pt; background-color: #d2edf4;" > 4. ORO FARINGE</td>
                <td width="18%"  border="1" style="font-size: 6pt;background-color:#d2edf4;">a. Labios</td>
                <td width="2%"  border="1"align="center">X</td>
                <td width="16%" border="1" style="font-size: 6pt;background-color:#d2edf4;"> d. Senos paranasales</td>
                <td width="2%"  border="1"align="center"> X </td>        
                <td width="16%" border="1" style="font-size:6pt;background-color:#d2edf4;">b. Pared abdominal</td>
                <td width="2%"  border="1"align="center"> X </td>      
                <td width="15%" border="1"style="font-size: 6pt;background-color: #d2edf4;"> b. Miembros superiores </td>
                <td width="2%"  border="1"align="center">X</td>      
            </tr>
            <tr>         
                <td class="campa-2">b. Conjuntivas</td>
                <td width="2%"  border="1"align="center">X</td>
                <td width="18%"  border="1" style="font-size: 6pt;background-color:#d2edf4;">b. Lengua</td>
                <td width="2%"  border="1"align="center">X</td>
                <td width="3%"  border="1" align="center" colspan="1" rowspan="2"  style="font-size: 6pt;  background-color: #d2edf4;" > 6. CUELLO</td>
                <td width="16%"  border="1"style="font-size: 6pt;  background-color: #d2edf4;" > a. Tiroides / masas</td>
                <td width="2%" border="1" align="center">X</td>
                <td width="3%"  border="1" align="center" colspan="1" rowspan="4" style="font-size: 6pt;  background-color: #d2edf4;" > 10. COLUMNA</td>
                <td width="16%"  border="1" style="font-size: 6pt;  background-color: #d2edf4;" >a. Flexibilidad</td>
                <td width="2%" border="1"align="center">X</td> 
                <td width="15%"  border="1" style="font-size: 6pt;background-color: #d2edf4;" > c. Miembros inferiores</td>
                <td width="2%"  border="1"align="center">X</td>
            </tr>
            
            <tr>         
                <td class="campa-2">c. Pupilas</td>
                <td width="2%"  border="1"align="center">X</td>
                <td width="18%" border="1" style="font-size:6pt;background-color:#d2edf4;">c. Faringe</td>
                <td width="2%"  border="1"align="center">X</td>
                <td width="16%"  border="1" style="font-size: 6pt;background-color:#d2edf4;">b. Movilidad</td>
                <td width="2%" border="1"align="center">X</td>
                <td width="16%" border="1" colspan="1" rowspan="2" style="font-size:6pt;background-color:#d2edf4;"> b. Desviación </td>
                <td width="2%"  border="1"align="center" colspan="1" rowspan="2">X</td>
                <td width="3%" border="1" align="center" colspan="1" rowspan="4" style="font-size:6pt;background-color: #d2edf4;"> 13. NEUROLOGICO</td>
                <td width="15%"  border="1" style="font-size:6pt;background-color:#d2edf4;">a. Fuerza</td>
                <td width="2%" border="1"align="center">X</td>   
            </tr>
            <tr>         
                <td class="campa-2">d. Córnea</td>
                <td width="2%"  border="1" align="centerD">X</td>
                <td width="18%" border="1" style="font-size:6pt;background-color:#d2edf4;">d. Amígdalas</td>
                <td width="2%" border="1"align="center">X</td>
                <td width="3%" border="1" align="center" colspan="1" rowspan="2" style="font-size: 6pt;  
                background-color:#d2edf4;">7.TORAX</td>
                <td width="16%" border="1" style="font-size:6pt;background-color: #d2edf4;"> a. Mamas</td>
                <td width="2%"  border="1"align="center">X</td>    
                <td width="15%" border="1" style="font-size:6pt;background-color: #d2edf4;"> b. Sensibilidad</td>
                <td width="2%" border="1"align="center">X</td>      
            </tr>
            <tr>        
                <td class="campa-2">e. Motilidad</td>
                <td width="2%"  border="1"align="center">X</td>
                <td width="18%"  border="1" style="font-size: 6pt;background-color: #d2edf4;" > e. Dentadura</td>
                <td width="2%"  border="1"align="center">X</td>
                <td width="16%"  border="1" style="font-size: 6pt;background-color:#d2edf4;">b. Corazón</td>
                <td width="2%"  border="1"align="center">X</td>
                <td width="16%"  border="1" style="font-size: 6pt;background-color:#d2edf4;">c. Dolor</td>
                <td width="2%"  border="1"align="center">X</td>
                <td width="15%" border="1" style="font-size:6pt;background-color:#d2edf4;">c. Marcha</td>
                <td width="2%"  border="1"align="center">X</td> 
            </tr>
            <tr>         
                <td width="87%" colspan="12" border="1" >SI EXISTE EVIDENCIA DE PATOLOGÍA MARCAR CON "X" Y DESCRIBIR EN LA SIGUIENTE SECCIÓN COLOCANDO EL NUMERAL</td>
                <td width="15%"  border="1" style="font-size: 6pt;  
                background-color: #ceffcc;"> d. Reflejos</td>
                <td width="2%"  border="1"align="center">x</td>
            </tr>
            <tr>
                <td  border="1" colspan="15" width="104%" align=""> Observaciones:</td>               
            </tr>
            <tr>
                <td  border="1" colspan="15" width="104%" align=""> FISICO REGIONAL OBSERVACION</td>               
            </tr> 
        </tbody>  
    </table>








<table class="table-mtv" border="1" width="100%">
        <tr>
            <th width="100%" class="camp-12-azul" colspan="50" >E.  FACTORES DE RIESGOS DEL PUESTO DE TRABAJO</th>
          </tr>
       
        <tr>
            
            <th class="camp-4" colspan="1" rowspan="2">
              PUESTO DE TRABAJO / ÁREA
          </th>
          <th class="camp-4" colspan="1" rowspan="2">
              ACTIVIDADES 
          </th>
          <th class="camp-4" colspan="1" rowspan="2">
              TIEMPO DE TRABAJO (meses)
          </th>
          <th class="camp-15" colspan="10" rowspan="1">FÍSICO</th>
           <th class="camp-15" colspan="15" rowspan="1">MECÁNICO</th>
           <th class="camp-15" colspan="9" rowspan="1">QUÍMICO</th>
           <th class="camp-15" colspan="6" rowspan="1">BIOLÓGICO</th>
           <th class="camp-15" colspan="6" rowspan="1"> ERGONÓMICO</th>
          
        </tr>
        <tr > 
            <th class="camp-verticalText" ><p class="camp-vertica" style="  ">Temperaturas altas</p></th>
            <th class="camp-verticalText" ><p class="camp-vertica" style="  ">Temperaturas bajas</p></th>
            <th class="camp-verticalText"><p class="camp-vertica">Radiación Ionizante</p></th>
            <th  class="camp-verticalText"><p class="camp-vertica">Radiación No Ionizante</p></th>
            <th class="camp-verticalText"><p class="camp-vertica">Ruido</p></th>
            <th class="camp-verticalText"><p class="camp-vertica" >Vibración</p></th>
            <th class="camp-verticalText"><p class="camp-vertica" >Iluminación</p></th>
            <th class="camp-verticalText"><p class="camp-vertica" >Ventilación</p></th>
            <th class="camp-verticalText"><p class="camp-vertica" >Fluido eléctrico</p></th>
            <th class="camp-verticalText"><p class="camp-vertica" >Otros _</p></th>



            <th class="camp-verticalText"><p class="camp-vertica">Atrapamiento entre máquinas</p></th>
            <th class="camp-verticalText"><p class="camp-vertica">Atrapamiento entre superficies</p></th>
            <th class="camp-verticalText"><p class="camp-vertica">Atrapamiento entre objetos </p></th>
            <th  class="camp-verticalText"><p class="camp-vertica">Caídas al mismo nivel</p></th>
            <th class="camp-verticalText"><p class="camp-vertica">Caídas a diferente nivel</p></th>
            <th class="camp-verticalText"><p class="camp-vertica" >Contacto eléctrico</p></th>
            <th class="camp-verticalText"><p class="camp-vertica" >Contacto con superficies de trabajos</p></th>
            <th class="camp-verticalText"><p class="camp-vertica" >Proyección de partículas – fragmentos</p></th>
            <th class="camp-verticalText"><p class="camp-vertica" >Proyección de fluidos</p></th>
            <th class="camp-verticalText"><p class="camp-vertica" >Pinchazos</p></th>
            <th class="camp-verticalText"><p class="camp-vertica" >Cortes</p></th>
            <th class="camp-verticalText"><p class="camp-vertica" >Atropellamientos por vehículos</p></th>
            <th class="camp-verticalText"><p class="camp-vertica" >Choques /colisión vehicular</p></th>
            <th class="camp-verticalText"><p class="camp-vertica" >Otros __</p></th>


             <th class="camp-verticalText"><p class="camp-vertica">Temperaturas altas</p></th>
            <th class="camp-verticalText"><p class="camp-vertica">Temperaturas bajas</p></th>
            <th class="camp-verticalText"><p class="camp-vertica">Radiación Ionizante</p></th>
            <th  class="camp-verticalText"><p class="camp-vertica">Radiación No Ionizante</p></th>
            <th class="camp-verticalText"><p class="camp-vertica">Ruido</p></th>
            <th class="camp-verticalText"><p class="camp-vertica" >Vibración</p></th>
            <th class="camp-verticalText"><p class="camp-vertica" >Iluminación</p></th>
            <th class="camp-verticalText"><p class="camp-vertica" >Ventilación</p></th>
            <th class="camp-verticalText"><p class="camp-vertica" >Fluido eléctrico</p></th>

        </tr>



        <tbody>
            <tr> 
                
                <td class="font-size6pt">emple</td>
                <td class="font-size6pt">empl}</td>
                <td class="font-size6pt">empl</td>
                <td class="font-size6pt">issriesg</td>
                <td class="font-size6pt"> (issriesg</td>
                <td class="font-size6pt"> (is</td>
                <td class="font-size6pt"> (isri</td>
                <td class="font-size6pt">(isies</td>
                <td class="font-size6pt"> (issig</td>
                <td class="font-size6pt">empleo</td> 
            </tr>
        </tbody>
    </table>

</body>
</html>
