<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use OPDF;
use setasign\Fpdi\Tcpdf\Fpdi;
use SetaPdf;
require_once('fpdf183/fpdf.php');
require_once('FPDI-2.3.6/src/autoload.php');
use SimpleSoftwareIO\QrCode\Facades\QrCode;


class PdfDemoController extends Controller
{
    public function index(){
        return view('PdfDemo');
    }

   
    // FUNCIONAL
    public function imprimir(){
    
    //funcion para generar nombre aleatorio unico
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    //variable de nombre aleatorio generado
    $nombrepdf = generateRandomString();
    //ruta donde se guardará el archivo
    $path = public_path($nombrepdf.'.pdf');
    
    //certificados
    $certificate = 'file://'.realpath('tcpdf.crt');
    $info = array(
        'Name' => 'TCPDF',
        'Location' => 'Office',
        'Reason' => 'Testing TCPDF',
        'ContactInfo' => 'http://www.tcpdf.org',
    );

    
    //CODIGO QR: se obtiene un codigo qr y se decodifica en base64
    $contenidoImagen = (QrCode::format('png')->size(100)->generate('HOLA JEAN'));
    $imagenBase64 = base64_encode($contenidoImagen);
    
    //generando pdf con DomPDF    
    $pdf = \OPDF::loadView('index2', compact('imagenBase64', 'var2'));
    //guardando pdf en ruta path    
    $pdf->save($path.'');
    $pdf -> download($nombrepdf.'.pdf');

    //generando pdf con FPDI extends TCPDF
    $pdf2 = new FPDI(); 
    $pdf2->AddPage('P', 'A4');
    $pdf2->setSourceFile($path); 
    $tplId = $pdf2 -> importPage(1);
    $pdf2 -> useTemplate($tplId, null,null,null,210,true);
    //BORRANDO ARCHIVO PATH
    unlink($path);
    //FIRMANDO DOCUMENTO
    $pdf2->setSignature($certificate, $certificate, 'tcpdfdemo', '', 2, $info); 
    $pdf2 -> Output("reporte.pdf", "I");
    }

    public function PDF077(){
    
    //funcion para generar nombre aleatorio unico
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    //variable de nombre aleatorio generado
    $nombrepdf = generateRandomString();
    //ruta donde se guardará el archivo
    $path = public_path($nombrepdf.'.pdf');
    
    //certificados
    $certificate = 'file://'.realpath('tcpdf.crt');
    $info = array(
        'Name' => 'TCPDF',
        'Location' => 'Office',
        'Reason' => 'Testing TCPDF',
        'ContactInfo' => 'http://www.tcpdf.org',
    );

    
    //CODIGO QR: se obtiene un codigo qr y se decodifica en base64
    $contenidoImagen = (QrCode::format('png')->size(100)->generate('HOLA JEAN'));
    $imagenBase64 = base64_encode($contenidoImagen);
    
    //generando pdf con DomPDF    
    $pdf = \OPDF::loadView('/pdf/077', compact('imagenBase64', 'var2'));
    //guardando pdf en ruta path    
    $pdf->save($path.'');
    $pdf -> download($nombrepdf.'.pdf', 'C');

    //generando pdf con FPDI extends TCPDF
    $pdf2 = new FPDI(); 
    // $pdf2->AddPage('P', 'A4');
    // $pdf2->setSourceFile($path); 
    // $tplId = $pdf2 -> importPage([1,2,3]);
    // $pdf2 -> useTemplate($tplId, null,null,null,210,true);
    

    $pageCount = $pdf2->setSourceFile($path);
    // iterate through all pages
    for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
    // import a page
    $pdf2->AddPage('P', 'A4');
    $templateId = $pdf2->importPage($pageNo);
    // use the imported page
    $pdf2->useTemplate($templateId);
    // $pdf2->SetXY(5, 5);  
}

    //BORRANDO ARCHIVO PATH
   // unlink($path);
   // gc_collect_cycles();
$gone = false;
for ($trial=0; $trial<10; $trial++) {
    if ($gone = @unlink($path)) {
        break;
    }
    // Wait a short time
    usleep(250000);
    // Maybe a concurrent script has deleted the file in the meantime
    clearstatcache();
    if (!file_exists($path)) {
        $gone = true;
        break;
    }
}
if (!$gone) {
    trigger_error('Warning: Could not delete file '.htmlspecialchars($path), E_USER_WARNING);
}


//unlink($path.'l'); 
// try { 
//     unlink($path).'l'; 
// } catch(Exception $e) { 
//     print "error!"; 
//     print $e;
// } 





    //FIRMANDO DOCUMENTO
    $pdf2->setSignature($certificate, $certificate, 'tcpdfdemo', '', 2, $info); 
    $pdf2 -> Output("reporte.pdf", "I");
    }

    public function PDF078(){
    
    //funcion para generar nombre aleatorio unico
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    //variable de nombre aleatorio generado
    $nombrepdf = generateRandomString();
    //ruta donde se guardará el archivo
    $path = public_path($nombrepdf.'.pdf');
    
    //certificados
    $certificate = 'file://'.realpath('tcpdf.crt');
    $info = array(
        'Name' => 'TCPDF',
        'Location' => 'Office',
        'Reason' => 'Testing TCPDF',
        'ContactInfo' => 'http://www.tcpdf.org',
    );

    
    //CODIGO QR: se obtiene un codigo qr y se decodifica en base64
    $contenidoImagen = (QrCode::format('png')->size(100)->generate('HOLA JEAN'));
    $imagenBase64 = base64_encode($contenidoImagen);
    
    //generando pdf con DomPDF    
    $pdf = \OPDF::loadView('/pdf/078', compact('imagenBase64', 'var2'));
    //guardando pdf en ruta path    
    $pdf->save($path.'');
    $pdf -> download($nombrepdf.'.pdf');

    //generando pdf con FPDI extends TCPDF
    $pdf2 = new FPDI(); 
    $pdf2->AddPage('P', 'A4');
    $pdf2->setSourceFile($path); 
    $tplId = $pdf2 -> importPage(1);
    $pdf2 -> useTemplate($tplId, null,null,null,210,true);
    //BORRANDO ARCHIVO PATH
    unlink($path);
    //FIRMANDO DOCUMENTO
    $pdf2->setSignature($certificate, $certificate, 'tcpdfdemo', '', 2, $info); 
    // $pdf2 -> Output("reporte078", "I");
        return view('pdf/077');
    }

    public function qr_generate(){
        $contenidoImagen = (QrCode::format('png')->size(100)->generate('Make me into QrCode!'));
        $imagenBase64 = base64_encode($contenidoImagen);
        
        $data = explode(',', $imagenBase64);
        $content = base64_decode($data[0]);
        // $cotizacion = [
        //     'jean'];
                $html_contect = '<h1> 
        PDF GENERADO USANDO TCPDF EN LARAVEL
        </h1>';
        PDF::SetTitle('Sample pdf');
        PDF::AddPage();
        $html = '<style>';
        $html .= '.verticalText {';
        $html .=  'writing-mode: vertical-lr;';
        $html .= 'transform: rotate(180deg);';
        $html .= '}';
        '</style>';
        $html = '<h1> 
        PDF GENERADO USANDO TCPDF EN LARAVEL
        </h1>';
        $html .= '<table border="1">';
        $html .= '<tr id="Cabecera_1">';
        $html .= '<th rowspan="2">Año</th>';
        $html .= '<th rowspan="2">Mes</th>';
        $html .= '<th rowspan="2">Día</th>';
        $html .= '</tr>';
        $html .= '<tr id="Cabecera_2">';
        $html .= '<th>Inicial</th>';
        $html .= '<th>Final</th>';
        $html .= '</tr>';
        $html .= '<tr id="2017_Abril_Viernes_28">';
        $html .= '<td rowspan="9"><p class="verticalText">2017</p></td>';
        $html .= '<td rowspan="3"><p class="verticalText">Abril</p></td>';
        $html .= '<td>Viernes</td>';
        $html .= '<td>28</td>';
        $html .= '<td>+</td>';
        $html .= '</tr>';
        PDF::writeHTML(view('index2',  ['cotiza' => $imagenBase64])->render());
        PDF::Output('SamplePDF.pdf' );
      
    }

    public function algoqrgenerate(){
        
        // $contenidoImagen = (QrCode::size(100)->generate('Make me into QrCode!'));
        // $imagenBase64 = base64_encode($contenidoImagen);
        
        // $data = explode(',', $imagenBase64);
        // $content = base64_decode($data[0]);
        // $cotizacion = [
        //     'jean'];
                $html_contect = '<h1> 
        PDF GENERADO USANDO TCPDF EN LARAVEL
        </h1>';
        PDF::SetTitle('Sample pdf');
        PDF::AddPage();
        PDF::writeHTML(view('index2')->render());
        PDF::Output('SamplePDF.pdf');
        // return view('index2');
    }

    public function savePDF(){
        
        $html_contect = '<h1> 
        PDF GENERADO USANDO TCPDF EN LARAVEL
        </h1>';
        PDF::SetTitle('Sample pdf');
        PDF::AddPage();
        PDF::writeHtml($html_contect, true,false,true,false, '');
        PDF::Output(public_path(uniqid().'_SamplePDF.pdf'), 'F');

    }

    public function downloadPDF(){
        $html_contect = '<h1> 
        PDF GENERADO USANDO TCPDF EN LARAVEL
        </h1>';
        PDF::SetTitle('Sample pdf');
        PDF::AddPage();
        PDF::writeHtml($html_contect, true,false,true,false, '');
        PDF::Output(public_path(uniqid().'_SamplePDF.pdf'), 'D');

    }

    public function HtmlToPDF(){
        $view = \View::make('index2');
        $html_contect = $view -> render();
        PDF::SetTitle('Sample pdf');
        PDF::AddPage();
        PDF::writeHtml($html_contect, true,false,true,false, '');
        PDF::Output(public_path(uniqid().'_SamplePDF.pdf'));

    }

    public function RESPALDO(){

        $certificate = 'file://'.realpath('tcpdf.crt');
        $info = array(
            'Name' => 'TCPDF',
            'Location' => 'Office',
            'Reason' => 'Testing TCPDF',
            'ContactInfo' => 'http://www.tcpdf.org',
        );

        $contenidoImagen = (QrCode::format('png')->size(100)->generate('Make me into QrCode!'));
        $imagenBase64 = base64_encode($contenidoImagen);


        $pdf3 = new \FPDF('portrait','mm', 'A4');
        $pdf3->AddPage();
        $pdf3->SetFont('Arial', 'B', 14);
        $text = 'This document is created with FPDF on '
            . date('r')
            . ' and digital signed with the SetaPDF-Signer component.';
        $pdf3->MultiCell(0, 8, $text);


        $fpdf = $pdf3->Output('', 'S');
      
        $pdf = new FPDI(); //FPDI extends TCPDF
        $pdf->AddPage('P', 'A4');
        $pdf -> SetFont('Arial', 'B', 14);
        $path = public_path('firmado.pdf');
        $pdf -> setSourceFile($path);
        $tplId = $pdf -> importPage(1);
        $pdf -> useTemplate($tplId);
        // $pdf -> Output('I', "demostest.pdf");
        $pdf2 = new \TCPDF();
        $pdf2 -> AddPage();
        $pdf2 -> writeHtml("FIRMADO");
        $pdf2->setSignature($certificate, $certificate, 'tcpdfdemo', '', 2, $info);

        $pdf2->Output($pdf -> Output('I', "demostest.pdf"), 'I');
              
    }

};

