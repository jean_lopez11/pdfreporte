<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{

    public function index()
    {
        return view('home');
    }


    public function index2()
    {
        return view('index2');
    }

    function subir_foto(Request $request){
        if (Input::hasFile('image')){
            $name = md5(rand(100, 200));
            $dir = public_path().'/ImageSummer/'; 
            $archivo=$request->image;        
            $fileName = $name.'.'.$archivo->getClientOriginalExtension();
            $archivo->move($dir, $fileName); 
            //return 'ImageSummer/'.$fileName;
            echo asset('ImageSummer/'.$fileName);
        }else{
            return -1;
        }
    }
}
